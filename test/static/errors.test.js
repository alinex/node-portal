const assert = require('assert')
const rp = require('request-promise')
const url = require('url')
const app = require('../../src/app')

const port = app.get('port') || 3030
const getUrl = pathname =>
  url.format({
    hostname: app.get('host') || 'localhost',
    protocol: 'http',
    port,
    pathname
  })

describe('Static: Error pages', () => {
  before(function(done) {
    this.server = app.listen(port)
    this.server.once('listening', () => done())
  })

  after(function(done) {
    this.server.close(done)
  })

  it('should show index page', () => {
    return rp(getUrl()).then(body =>
      assert.ok(body.indexOf('<h1 class="center-text">Administration Portal</h1>') !== -1)
    )
  })

  describe('404 error', function() {
    it('should return 404 HTML page', () => {
      return rp({
        url: getUrl('path/to/nowhere'),
        headers: {
          Accept: 'text/html'
        }
      }).catch(res => {
        assert.ok(res.error.indexOf('<h2 class="center-text">404</h2>') !== -1)
      })
    })

    it('should return 404 JSON error', () => {
      return rp({
        url: getUrl('path/to/nowhere'),
        json: true
      }).catch(res => {
        assert.equal(res.statusCode, 404)
        assert.equal(res.error.code, 404)
        assert.equal(res.error.name, 'NotFound')
        assert.equal(res.error.message, 'Page not found')
      })
    })
  })
})
