const assert = require('assert')
const rp = require('request-promise')
const url = require('url')
const app = require('../../src/app')

const port = app.get('port') || 3030
const getUrl = pathname =>
  url.format({
    hostname: app.get('host') || 'localhost',
    protocol: 'http',
    port,
    pathname
  })

describe('Static: Web client', () => {
  before(function(done) {
    this.server = app.listen(port)
    this.server.once('listening', () => done())
  })

  after(function(done) {
    this.server.close(done)
  })

  it('should get SPA', () => {
    return rp(getUrl('/web')).then(body => {
      assert.ok(
        body.indexOf(
          '<noscript>This is your fallback content in case JavaScript fails to load.</noscript>'
        ) !== -1
      )
    })
  })
})
