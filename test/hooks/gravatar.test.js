const assert = require('assert')
const feathers = require('@feathersjs/feathers')
const gravatar = require('../../src/hooks/gravatar')

describe.skip('\'gravatar\' hook', () => {
  let app

  beforeEach(() => {
    app = feathers()

    app.use('/dummy', {
      async get(id) {
        return { id }
      }
    })

    app.service('dummy').hooks({
      before: gravatar()
    })
  })

  it('runs the hook', async () => {
    // A user stub with just an `_id`
    const user = { _id: 'dummy', email: 'info@alinex.de' }
    // The service method call `params`
    const params = { user }
    const result = await app.service('dummy').get('demo')

    //assert.deepEqual(result, { id: 'test' })
  })
})
