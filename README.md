# Alinex Portal Server

**This repository is deprecated, see the [Alinex Server](https://gitlab.com/alinex/node-server) which is a rebuild of it.**

This is the server used for the **Administration Portal**. It's an interface to do administration tasks easy and fast. It should be a base for specialized tools for specific environments which can be customized to it's needs.

It also contains the hosted [Client](https://gitlab.com/alinex/node-portal-client) which is used to access the server from it's webpanel, desktop application or mobile apps.

## Documentation

Find a complete manual under [alinex.gitlab.io/node-portal](https://alinex.gitlab.io/node-portal).

## Demo

Check out the demo on a small shared host: [alinex.peacock.uberspace.de](http://alinex.peacock.uberspace.de).

## Highlights

This application is an optimal combination of different frameworks and packages to make the complete application round, consistent and complete. It is not only packed together but also optimized and pre configured to work.

In the end the result is a modern web frontend for nearly any task.

_Alexander Schilling_
