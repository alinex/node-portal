# Last Changes

## Newest Changes

-   logging with IP for all events
-   update server and client modules
-   add swagger authentication using APIkey

## Version 0.2.0 (10.08.2018)

-   user and role management
-   information service and client display
-   http and service logging
-   JWT authentication
-   selfcheck system
-   client based on Quasar Framework
-   REST server based on feathers.js
-   initial version
