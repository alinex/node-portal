# Hooks

Hooks are pluggable middleware functions that can be registered before, after or on errors of a service method. You can register a single hook function or create a chain of them to create complex work-flows.

Hooks are commonly used to handle things like validation, logging, populating related entities, sending notifications and more. This pattern keeps your application logic flexible, composable, and much easier to trace through and debug. 

They are linked into 

- the application/service to be run
- before / after or on error
- on all or an specific method
- as a list of functions to be called

## Context

The hook `context` is passed to a hook function and contains information about the service method call.

Read only information:

- `app` - the Feathers application object to retrieve logger or othger global elements
- `service` - the service this hook currently runs on
- `path`- the service name (or path) without leading or trailing slashes
- `method`- the name of the service method (one of `find`, `get`, `create`, `update`, `patch`, `remove`)
- `type` - the hook type (one of `before`, `after` or `error`)

Changeable information:

- `params` - the service method parameters (including `params.query`)
- `id` - the id for a `get`, `remove`, `update` and `patch` service method call, it can be also be `null` when modifying multiple entries
- `data` - the request data of a `create`, `update` and `patch` service method call
- `error` - the error object that was thrown in a failed method call
- `result` - the result of the successful service method call, only available in `after` hooks or set in before hook to skip the actual service method
- `dispatch` - optional property containing a "safe" version of the data that should be sent to any external client instead of `result`
- `statusCode` - optional property that allows to override the standard HTTP status code that should be returned




Common hooks are:

- logger - used for debugging and logs the service call before or after
- populate - integrate referenced documents
- depopulate - remove integrated documents

Access control:

- authenticate - login
- authorize - check access rights

Specific Hooks:

- gravatar - adds gravatar url as `avatar` from `email`
