# REST API

The concrete API is displayed through the included [Swagger](https://swagger.io/) API documentation which you can access through the `/swagger` URL.

The server may be called through plain HTTP Requests or using the Feathers Client through Socket.io. The following description shows both types.

| Service   | HTTP method | Path        |
| --------- | ----------- | ----------- |
| .find()   | GET         | /messages   |
| .get()    | GET         | /messages/1 |
| .create() | POST        | /messages   |
| .update() | PUT         | /messages/1 |
| .patch()  | PATCH       | /messages/1 |
| .remove() | DELETE      | /messages/1 |

To help working on the command line install `jq` a toll to display JSON and query it on the command line:

```bash
sudo apt-get install jq
```

Now you can call any REST message using curl and display it in readable form:

```bash
curl -sX GET http://localhost:3030/messages/ | jq
```

```json
{"total":0,"limit":10,"skip":0,"data":[]}
```

## Find

Retrieves a list of all matching resources from the service:

```text tab="HTTP"
GET /info
```

```js tab="JavaScript"
app.service('info').find();
```

```text tab="bash"
curl -sX GET http://localhost:3030/info | jq
```

```json tab="Response"
[
  {
    "group": "host",
    "name": "architecture",
    "value": "x64"
  },
  {
    "group": "host",
    "name": "cpu type",
    "value": "Intel(R) Core(TM) i7-6500U CPU @ 2.50GHz"
  },
  ...
]
```

### Filtering

You can also send additional filtering constraints on specific field values.
To get only records with 'group' equal 'node':

```text tab="HTTP"
GET /info?group=node
```

```js tab="JavaScript"
app.service('info').find({
    query: {
        group: 'node'
    }
});
```

```text tab="bash"
curl -sX GET http://localhost:3030/info?group=node | jq
```

```json tab="Response"
[
  {
    "group": "node",
    "name": "process id",
    "value": 7615
  },
  {
    "group": "node",
    "name": "parent process id",
    "value": 7614
  },
  ...
]
```

You may also use any of the built-in find operands ($le, $lt, $ne, $eq, $in, etc.) the general format is as follows.

| Operand | Usage                         | Comment                                 |
| ------- | ----------------------------- | --------------------------------------- |
| -       | `field: value`                | Simple equality                         |
| `$ne`   | `field: {$ne: value}`         | Field is not equal value                |
| `$in`   | `field: {$in: [value, ...]}`  | In list of values                       |
| `$nin`  | `field: {$nin: [value, ...]}` | Not in list of values                   |
| `$lt`   | `field: {$lt: value}`         | Field lower than given value            |
| `$lte`  | `field: {$lt: value}`         | Field lower or equal than given value   |
| `$gt`   | `field: {$lt: value}`         | Field greater than given value          |
| `$lte`  | `field: {$lt: value}`         | Field greater or equal than given value |

For example, to find all records which are not in group 'node':

```text tab="HTTP"
GET /info?name[$ne]=node
```

```js tab="JavaScript"
app.service('info').find({
    query: {
        name: {
            $ne: 'node'
        }
    }
});
```

```text tab="bash"
curl -sX GET http://localhost:3030/info -G \
--data-urlencode 'name[$ne]=node' \
| jq
```

```json tab="Response"
[
  {
    "group": "host",
    "name": "architecture",
    "value": "x64"
  },
  {
    "group": "host",
    "name": "cpu type",
    "value": "Intel(R) Core(TM) i7-6500U CPU @ 2.50GHz"
  },
  ...
]
```

You can also specify multiple fields which have to be all matching the defined parameters.
That's like an _and_ search. To make alternative groups of restrictions use `$or`:

```text tab="HTTP"
GET /info?$or[0][group][$ne]=node&$or[1][name]=cpu
```

```js tab="JavaScript"
app.service('info').find({
    query: {
        $or: [
            { group: { $ne: 'node' } },
            { name: 'cpu' }
        ]
    }
});
```

```text tab="bash"
curl -sX GET http://localhost:3030/info -G \
--data-urlencode '$or[0][name][$ne]=node' \
--data-urlencode '$or[1][name]=cpu' \
| jq
```

```json tab="Response"
[
  {
    "group": "host",
    "name": "architecture",
    "value": "x64"
  },
  {
    "group": "host",
    "name": "cpu type",
    "value": "Intel(R) Core(TM) i7-6500U CPU @ 2.50GHz"
  },
  ...
]
```

### Limit

Most services support pagination here with the additional parameters. If nothing is
given the preconfigured default will be used.

`$limit` will return only the number of results you specify.
Retrieves the first ten values:

```text tab="HTTP"
GET /info?$limit=10
```

```js tab="JavaScript"
app.service('info').find({
    query: {
        $limit: 2
    }
});
```

```text tab="bash"
curl -sX GET http://localhost:3030/info -G \
--data-urlencode '$limit=2' \
| jq
```

```json tab="Response"
[
  {
    "group": "host",
    "name": "architecture",
    "value": "x64"
  },
  {
    "group": "host",
    "name": "cpu type",
    "value": "Intel(R) Core(TM) i7-6500U CPU @ 2.50GHz"
  }
]
```

If you want to get only the number of records you may also set `$limit` to `0`.
This ensures that no record is retrieved but you get the meta info like totalmemin the returned page object.

### Offset (skip)

`$skip` will skip the specified number of results. If you skip 2 records the result
will start with record number 3, so to show record 3 and 4:

```text tab="HTTP"
GET /info?$limit=2&$skip=2
```

```js tab="JavaScript"
app.service('info').find({
    query: {
        $limit: 2,
        $skip: 2
    }
});
```

```text tab="bash"
curl -sX GET http://localhost:3030/info -G \
--data-urlencode '$limit=2' \
--data-urlencode '$skip=2' \
| jq
```

```json tab="Response"
[
  {
    "group": "host",
    "name": "cpu cores",
    "value": 4
  },
  {
    "group": "host",
    "name": "cpu speed (MHz)",
    "value": 2730
  }
]
```

### Sorting

`$sort` will sort based on the object you provide. It can contain a list of properties
by which to sort mapped to the order (1 ascending, -1 descending).

```text tab="HTTP"
/info?$sort[name]=1
```

```js tab="JavaScript"
app.service('info').find({
    query: {
        $sort: {
            name: 1
        }
    }
});
```

```text tab="bash"
curl -sX GET http://localhost:3030/info -G \
--data-urlencode '$sort[name]=1' \
| jq
```

```json tab="Response"
[
  {
    "group": "env",
    "name": "COLORTERM",
    "value": "truecolor"
  },
  {
    "group": "env",
    "name": "DBUS_SESSION_BUS_ADDRESS",
    "value": "unix:abstract=/tmp/dbus-NdF5Pg7sjt,guid=e0a8fb125f75f2636fe7bdf45c912466"
  },
  ---
]
```

### Select columns

`$select` allows to pick which fields to include in the result.

To only retrieve the fields 'name' and 'value' but not the group call:

```text tab="HTTP"
GET /info?$select[]=name&$select[]=value
```

```js tab="JavaScript"
app.service('info').find({
    query: {
        $select: [ 'name', 'value' ]
    }
});
```

```text tab="bash"
curl -sX GET http://localhost:3030/info -G \
--data-urlencode '$select[]=name' \
--data-urlencode '$select[]=value' \
| jq
```

```json tab="Response"
[
  {
    "name": "architecture",
    "value": "x64"
  },
  {
    "name": "cpu type",
    "value": "Intel(R) Core(TM) i7-6500U CPU @ 2.50GHz"
  },
  {
    "name": "cpu cores",
    "value": 4
  },
  ...
]
```

## Get

Retrieve a single resource from the service by its ID:

```text tab="HTTP"
GET /check/base
```

```js tab="JavaScript"
app.service('check').get('base');
```

```text tab="bash"
curl -sX GET http://localhost:3030/check/base | jq
```

```json tab="Response"
{
  "check": "base",
  "status": true,
  "message": "Base check that server is running",
  "time": 0
}
```

`$select` allows to pick which fields to include in the record:

```text tab="HTTP"
GET /check/base?$select[]=message
```

```js tab="JavaScript"
app.service('check').get('base', {
    query: {
        $select: [ 'message' ]
    }
});
```

```text tab="bash"
curl -sX GET http://localhost:3030/check/base -G \
--data-urlencode '$select[]=message' \
| jq
```

## Create

Create a new resource with data.

```text tab="HTTP"
POST /messages
{ "text": "I really have to iron" }
```

```js tab="JavaScript"
app.service('messages').create(
    { "text": "I really have to iron" }
);
```

You may also create multiple records in one call:

```text tab="HTTP"
POST /messages
[
    { "text": "I really have to iron" },
    { "text": "Do laundry" }
]
```

```js tab="JavaScript"
app.service('messages').create([  
    { "text": "I really have to iron" },
    { "text": "Do laundry" }
]);
```

## Update

Completely replace a single or multiple resources.

Given an ID the specified record will be replaced:

```text tab="HTTP"
PUT /messages/2
{ "text": "I really have to do laundry" }
```

```js tab="JavaScript"
app.service('messages').update(2,  
    { "text": "I really have to do laundry" }
);
```

## Patch

Merge the existing data of a single or multiple resources with the new data.

```text tab="HTTP"
PATCH /messages/2
{ "read": true }
```

```js tab="JavaScript"
app.service('messages').patch(2,  
    { "read": true }
);
```

When no id is given patch all records or select some using query parameters:

```text tab="HTTP"
PATCH /messages?complete=false
{ "complete": true }
```

See the description for filtering in the `find` method above.

## Remove

Remove a single or multiple resources.

If an id is given only this will be removed.

```text tab="HTTP"
DELETE /messages/2
```

But you may also call it without an id and a filter definition:

```text tab="HTTP"
DELETE /messages?read=true
```

See the description for filtering in the `find` method above.

## Errors

As an example we call a service which didn't implement the GET method, only the FIND method:

```text tab="HTTP"
GET /info/1
```

```js tab="JavaScript"
app.service('info').get(1);
```

```text tab="bash"
curl -sX GET http://localhost:3030/info/1 | jq
```

```json tab="Response"
{
  "name": "MethodNotAllowed",
  "message": "Method `get` is not supported by this endpoint.",
  "code": 405,
  "className": "method-not-allowed",
  "errors": {}
}
```
