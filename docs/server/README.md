title: Overview

# Portal Server

This is the server used for the **Alinex Portal**. It's an interface to do administration tasks easy and fast. It should be a base for specialized tools for specific environments which can be customized to it's needs.

The server part is developed as [alinex-portal](https://gitlab.com/alinex/node-portal) package.

In it the ready build [Client](https://gitlab.com/alinex/node-portal-client) can be included to access the server from it's webpanel, desktop application or mobile apps.

## Features

-   stateless using simple Java Web Token for authentication
-   websocket (Realtime API) or HTTP REST
-   service oriented architecture
-   NodeJS 8
-   server validation
-   multiple database support
-   logging events
-   Swagger and Logtail in Browser

## Development System

Fork the repository from [Gitlab](https://gitlab.com/alinex/node-portal).

```bash
export NODE_ENV=development
npm run dev   # start the development server
npm run test  # to run the automatic tests and linter
npm start     # start in productive mode
```

If you get noticed about too much files to watch enhance this system setting using:

    sudo echo 100000000 | sudo tee /proc/sys/fs/inotify/max_user_watches

Then change it to your needs.

The development of the server is aimed to NodeJS 8.x supporting most ES2017 but not the module loading. It is directly used without transpiling.

### Testing

You may run one of the shortcut commands for testing:

```bash
npm run test    # run eslint and mocha, one after the other
npm run eslint  # only check using eslint
npm run mocha   # only run the unit test cases
```

## Technologies

-   [ExpressJS](http://expressjs.com/de/) - webserver
-   [Feathers](https://feathersjs.com/) - REST and realtime API
-   [Winston](https://github.com/winstonjs/winston) - logging
-   [mongoose](http://mongoosejs.com/) - database connection
-   [feathers-mongoose](https://github.com/feathersjs-ecosystem/feathers-mongoose) - Model helper
-   [Authentication](https://docs.feathersjs.com/api/authentication/server.html) - JWT support
-   [CASL](https://stalniy.github.io/casl/) - Authorization library
