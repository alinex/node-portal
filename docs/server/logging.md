# Logging

The server will write access logging to standard output and error logging to stderr on the calling command. But if started using `npm start` this will both piped into the log files:

    2019-03-22 18:49:10.077 [info   ] Initialization done - http://192.168.178.22:3030
    2019-03-22 18:49:11.776 [info   ] ::ffff:127.0.0.1 HTTP before GET /check/base?$select[]=message curl/7.58.0
    2019-03-22 18:49:11.779 [debug  ] ::ffff:127.0.0.1 HTTP Header: { host: 'localhost:3030', 'user-agent': 'curl/7.58.0', accept: '*/*' }
    2019-03-22 18:49:11.785 [info   ] ::ffff:127.0.0.1 REST before GET /check/base
    2019-03-22 18:49:11.786 [debug  ] ::ffff:127.0.0.1 REST Header: { host: 'localhost:3030', 'user-agent': 'curl/7.58.0', accept: '*/*' }
    2019-03-22 18:49:11.786 [debug  ] ::ffff:127.0.0.1 REST Parameter: { '$select': [ 'message' ] }
    2019-03-22 18:49:11.786 [info   ] ::ffff:127.0.0.1 REST after  GET /check/base
    2019-03-22 18:49:11.787 [debug  ] ::ffff:127.0.0.1 REST Result: { check: 'base', status: true, message: 'Base check that server is running', time: 0 }
    2019-03-22 18:49:11.793 [info   ] ::ffff:127.0.0.1 HTTP after  GET /check/base?$select[]=message 200 OK 18 ms

In the example above you see a check call procedure in debug logging. Each call has a `before` and `after` entry. While you find the request data below each `before` call the response data is shown below each `after` log entry. Internal calls to the API are logged as `VERBOSE`.

Logging lines contains the following information:

-   date
-   logging level in square brackets
    -   `error` - hard error there the system won't work correctly
    -   `warn` - problems which are not critical and you may work on
    -   `info` - mandatory information like server start and external requests
    -   `verbose` - internal rewuests and processing information
    -   `debug` - show request headers, parameters and response values
    -   `silly` - seldom used for specific deep debugging of specific parts
-   message

The message itself can be of different types:

-   requests
    -   client IP or `-`
    -   used protocol (HTTP, HTTPS, SOCKETIO, REST, INTERNAL)
    -   type (before or after)
    -   method (GET, FIND, CREATE, UPDATE, PATCH, REMOVE)
    -   path
    -   user agent for HTTP before only
    -   return code and request time for HTTP after only
-   request data
    -   client IP or `-`
    -   type of information followed by colon
    -   information itself
-   processing information

Logging can be into console output or into file:

-   daily rotating file
-   automatic compression
-   separate error log

See the [configuration](config.md) for how to change the loglevel from will which you will get log messages and there to log to.
