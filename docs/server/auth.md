# Access Control

Access to data and services is restricted by authentication and authorization using user, roles and abilities. We combine here a Role Based Access Control (RBAC) with Attribute Based Access Control (ABAC).

Role Based Access Control (RBAC) relies on the grouping of users into various roles which are then assigned rights. A right is typically made up of an action and a resource type, e.g. role manager can create (action) documents (resource type). Roles in RBAC systems are usually considered as static.

Attribute Based Access Control (ABAC), allows to enforce authorization decisions based on any attribute accessible to your application and not just the user’s role. This could be an attribute of the subject, the action he is performing, the resource he is trying to manipulate, or even the context. The combination of these four things allows for policies to be set up that are as fine-grained as you desire. Roles in ABAC systems are dynamic, a unique role might even be generated for each user.

The downside of ABAC is that administration gets complex and overview of who is allowed for what is not as easy. Therefore we define ABAC within central roles and give them to the users.

## Storage

All information to do authorization and authentication is stored in mongo DB collections: `users` and `roles`. Find out more about these in the administration services: [users](../services/users.md) and [roles](../services/roles.md).

## Authentication

As displayed in the graphic the user may come with Login Data, an JSON Web Token (JWT) or nothing. The authentication will identify the user and check it'S identification against validity.

To make authentication easy and secure [JWT](https://auth0.com/docs/jwt) is used. This allows to use stateless server cluster and be easy to scale.

!!! info

    Only internal calls, authentication and the `check` service from localhost can be called without any access control checking.

### JSON Web Token (JWT)

It is defined as an open standard in [RFC 7519](https://tools.ietf.org/html/rfc7519) and defines a compact and self-contained way for securely transmitting information between parties as a JSON object. This information can be verified and trusted because it is digitally signed.

The JWT defines a compact and self-contained way for securely transmitting information between parties as a signed JSON object consisting of three parts:

-   Header
-   Payload
-   Signature

Therefore, a JWT typically looks like the following with all three parts being Base64 encoded:

    xxxxx.yyyyy.zzzzz

To easily analyze your JWT you may let it's payload and header be displayed using the [online decoder](https://devtoolzone.com/decoder/jwt).

#### Header

The header typically consists of two parts: the type of the token, which is JWT, and the hashing algorithm being used, such as HMAC SHA256 or RSA.

```json
{
    "alg": "HS256",
    "typ": "JWT"
}
```

#### Payload

The second part of the token is the payload, which contains the claims. Claims are statements about the user.

The JWT specification defines seven claims that can be included in a token. These are registered claim names, and they are:

    iss - issuer: identifies the principal that issued the JWT
    sub - subject: identifies the principal that is the subject of the JWT
    aud - audience: identifies the recipients that the JWT is intended for as
          string or array
    exp - expirarion time: identifies the expiration time on or after which the
          JWT MUST NOT be accepted for processing
    nbf - not brfore: identifies the time before which the JWT MUST NOT be accepted
          for processing
    iat - issued at: identifies the time at which the JWT was issued
    jti - unique identifier: provides a unique identifier for the JWT

We use the following private claim names for essential user data:

    userid - alphanumeric id string
    email - email address
    nickname - casual name
    avatar - link to avatar image
    abilities - array with allowed actions

An example of payload could be:

```json
{
    "iss": "alinex.de",
    "sub": "portal",
    "aud": "https://demo.alinex.de",
    "iat": 1553897072,
    "exp": 1553983472,
    "jti": "274b9fa4-82fc-4868-aa78-337323b63b42",
    "userId": "429ee71276122f55a3a94796",
    "email": "demo@alinex.de",
    "nickname": "demo",
    "avatar": "https://www.gravatar.com/avatar/dc23461c186acb6dacd8e6137543fdec?s=60&d=mm",
    "abilities": [
        {
            "actions": ["read", "update", "create", "remove"],
            "subject": ["users", "roles", "info"],
            "fields": []
        }
    ]
}
```

#### Signature

To create the signature part you have to take the encoded header, the encoded payload, a secret, the algorithm specified in the header, and sign that like:

```js
HMACSHA256(base64UrlEncode(header) + '.' + base64UrlEncode(payload), secret);
```

The signature is used to verify that the sender of the JWT is who it says it is and to ensure that the message wasn't changed along the way. Only the server knows the secret so no one can make a valid JWT as the server itself and only accepts his own JWT.

As an example, if the JWT is changed on the way to the client or back on the next action the server will reject.

### Setup the JWT payload

The JWT payload is specified within the `middleware/authentication.js`:

```js
app.service('authentication').hooks({
    before: {
        create: [
            authentication.hooks.authenticate(config.strategies),
            context => {
                // make sure params.payload exists
                context.params.payload = context.params.payload || {};
                // merge in additional properties
                Object.assign(context.params.payload, {
                    // add all which may be used in the client or middleware servers
                    // but keep it as small as possible
                });
            }
        ],
        remove: [authentication.hooks.authenticate('jwt')]
    }
});
```

Keep in mind that the payload is sent on every request/response. So keep it as small as possible. Also the defined secret (see [configuation](../server/config.md)) have to be larger than the payload for increased security.

### Login

Now you may get the JWT:

```bash
$ curl -sX \
    POST http://localhost:3030/authentication \
    -H 'Content-Type: application/json' \
    --data-binary '{ "strategy": "local", "email": "demo@alinex.de", "password": "demo123" }' \
    | jq
```

```json
{
    "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI0MjllZTcxMjc2MTIyZjU1YTNhOTQ3OTYiLCJlbWFpbCI6ImRlbW9AYWxpbmV4LmRlIiwiaWF0IjoxNTUyNTEwOTEwLCJleHAiOjE1NTI1OTczMTAsImF1ZCI6Imh0dHBzOi8vZGVtby5hbGluZXguZGUiLCJpc3MiOiJhbGluZXguZGUiLCJzdWIiOiJwb3J0YWwiLCJqdGkiOiIxYTg3ZjEyYy1lZWZkLTQwOTYtOWQxZi0yM2YwZWZjYTAyOWUifQ.jSnXb52WHBy8CcxAN8BJoaPWxd8tcs4_ELGl-Kvj9Bw"
}
```

To work easy within the shell I will store the token in a shell variable:

```bash
$ export JWT=$(curl -sX \
    POST http://localhost:3030/authentication \
    -H 'Content-Type: application/json' \
    --data-binary '{ "strategy": "local", "email": "demo@alinex.de", "password": "demo123" }' \
    | jq -r .accessToken )
```

This access Token can now be used to access restricted services:

```bash
$ curl -H "Authorization: Bearer $JWT" -sX \
    GET http://localhost:3030/messages \
    | jq
```

## Authorization

After the user is identified the authorization will check the user's abilities. Which is what the user is allowed to do. This is also stored in the payload of the JWT.

### Roles

For easy administration the user rights are specified through roles which combine some abilities to a named group. Each user can now be set on multiple roles, which define what he is allowed to do.

-   name: string
-   description?: string
-   disabled?: flag // default is `false`
-   abilities: list

The roles are stored in the MongoDB collection `roles` containing also the defined abilities as a list.

### Abilities

An ability is a right to do something. It contains:

-   actions: 'read', 'update', 'create', 'remove'
-   subject: name
-   conditions?: { field: check, ... }
-   fields?: name, ...
-   inverted?: flag // default is `false`
-   reason?: string // mainly to specify why user can't do something. See forbidden reasons for details

Within the conditions the following special values may be used:

-   `$MYSELF` which is replaced by the current user id
