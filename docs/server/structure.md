# Structure

The following files are on a production server:

    bin             # binaries to start the server
    conf            # configuration
    logs            # log files
    public          # static files
    node_modules    # third party node modules

The development system will also contain:

    .vscode         # editor config
    src             # code sources
    test            # unit tests
    docs            # documentation sources

## Binaries

The `/bin` directory contains a shell script to easily run the server in production mode.

## Configuration

Within the `/config` directory the following configurations:

    default.json      # base configuration
    production.json   # used if run with NODE_ENV=production (`npm start`)

In this files you may change something like the `hostname`, `port` or the authentication.
More complex things should be changed in the code.

## Logs

Log files in `logs` are stored as:

-   daily rotating file
-   automatic compression
-   separate error log
-   cleaned up after some time

## Static, public

Within the `/public` path I have all files which will be served directly as they are:

    index.html        # the homepage
    web/              # the web client (node-portal-client)
    download/         # links to desktop app downloads
    error/            # error pages will be used on demand
      401.html
      404.html
      default.html

To integrate the development web client the `web` directory is a softlink to the client's `dist` directory.

## Codebase

Under `src` you will find all sources.

The base files are:

    index.js            # Start script used to run the server
    app.js              # Setup of the express server and the middleware
    app.hooks.js        # General hooks
    logger.js           # Setup for winston logger
    authentication.js   # Setup of authentication
    channel.js          # setup of communication chanels to the client

And then the concrete logic is split in the following directories:

    middleware          # for additional middleware
    services            # all the REST services
    hooks               # scripts to be used in hook setups
    models              # data models

### Middleware

This are additional components which are loaded globally. They add general plugins into the application or also integrate completely selfdefined routes.

Setup general plugins:

-   mongoose connection
-   authentication

Add specific addon applications:

-   logtail
-   swagger

### Services

Each service is organized as a directory and contains at least three files:

    index.js           # the service setup
    hooks.js           # linking to hook scripts
    api.js             # API documentation (used by swagger)

To have a quick look what is going on, you may call the `/logtail` middleware to
check the last lines in the log.

### Hooks

Hooks are handler which are globally stored to be used in multiple services.
They are loaded into the service and referenced in it's hooks.

Common hooks are:

-   logger - used for debugging and logs the service call before or after
-   populate - integrate referenced documents
-   depopulate - remove integrated documents

Access control:

-   authenticate - login
-   authorize - check access rights

### Models

Data models for object bridges for the data store like mongoDB or RDBMS.

## Test

Unit Tests are partly available under `/test` but you may also consider using the integrated tests of the server itself under the `check` service.

## Docs

The documentation is written under `/docs` in markdown format. Use `mkdocs` to create an HTML representation of it.
