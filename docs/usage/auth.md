title: Access Control

# Authentication and authorization

For most elements within the system you have to be authenticated by logging in using your email address and password.

![Access Entities](../entity-access.svg)

Behind the scene an authorization system will hold all allowed abilities for you and will limit your work to only these. You only be able to see and use the elements you are allowed to.

## Administration

Just from the start the system comes with three basic roles:

-   Guest - show info, login or create account
-   User - show info, edit own user infos, login / logout
-   Admin - anything allowed

But this can be completely changed by you through the administration dialogs described below. To do this you need full rights on roles and users as set in the 'Admin' role.

Each user or role may be disabled which will work immediately.

### User Administration

To allow anybody to use the restricted parts an user account is needed. As long as no registration is available an administrator have to create the user and set an initial password. Use the dialog under core/users therefore.

![Edit Users](users.png)

To define what the user is allowed to do you have to add the specific roles which contains the correct access rights.

### Role Administration

Additionally to the base roles like 'administrator' you may add individual roles under core/roles.

![Edit Roles](roles.png)

Here you have to define what is allowed for users which has this role. The definition is based on:

-   `actions` - they define the operations which are allowed like 'read', 'update', 'create', 'delete'
-   `subject` - specifies the object (API name) on which to operate
-   `conditions` - can be used to further limit the rule to only the records matching the specified constraints
-   `fields` - can be used to only allow to work on some fields of this object
-   `inverted` - if this is set the rule will not allow but disallow

Use the inverted rule to allow something but disallow a part of it, like delete all users but not the administrator.
