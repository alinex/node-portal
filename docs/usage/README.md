title: Introduction

# Manual

This part will guide you through all that is necessary to really use the portal. It starts with the installation steps and will guide you through the administration and setup.

For normal use you don't need to know anything as everything should be real self explanatory. But I nevertheless I will explain the basics here. But be patient this part will not be complete unless the complete system is stable and productive under use.

Also keep in mind that the system is not a fully blown application here but a starting point for an individual interface. So all details about the real contents which are added later can not be contained herein.
