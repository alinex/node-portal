# Basics

## Info

The info dialog contains a lot of information about all parts of the system. This may be helpful in case of problems to give some of these data to the support staff.

## Validation

Before submitting and also just as you leave a field a basic validity check is fulfilled. It will immediately inform xou just below the field that something is wrong. But as you send it the server will also make a further check which will be displayed in response as a short note at the bottom.
