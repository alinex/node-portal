# Installation

Attention, this isn't a fully blown application but more a work base for your needs.
So mostly you have to install the development version, configure and build your system and deploy it.

This base system will give you all the things from the [demo.alinex.de](https://demo.alinex.de) but not more.

## Install Server

To get the server up and running you have to install it on a linux system:

```bash
$ curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
$ sudo apt-get install -y nodejs
```

At the moment there is no NPM package but I will make one later. This should be simply installable by:

```bash
$ npm install -g alinex-portal
```

Alternatively you may also download and install the code from github:

```bash
$ clone https://gitlab.com/alinex/node-portal
$ cd node-portal
$ npm install
```

Setup live system:

```bash
$ echo "export NODE_ENV=production" >> ~/.bash_profile
$ source ~/.bash_profile
```

### Update Server

To update an already installed server from Git do the following:

```bash
# remove local changes
git stash save --keep-index
git stash drop
# get new code and modules
git pull
npm install
```

Afterwards the client have to be copied again (see below).

## Install MongoDB Server

As some modules use it as data store you have to use a local or remote MongoDB server:

```bash
$ sudo apt-get install mongodb
```

This will install MongoDB. Afterwards you have to create a first user account by hand to be able to login. The following code shows how to do so with the included start records.

```bash
$ mongo alinexDB < bin/setup.mongo
MongoDB shell version: 2.6.10
connecting to: alinexDB
WriteResult({ "nInserted" : 1 })
```

If you need host, port and user then call it like:

```bash
$ mongo admin --host localhost --port 30222 -u alinex_mongoadmin -p kapu873jud < bin/setup.mongo
```

You may also use another name for the database instead of `alinexDB`. But you have to change it in the configuration, too.

Now you have the default user and rights and are able to do the rest online through the system itself. Use the login displayed as placeholder in the form and delete this user later.

If you use another address for your mongodb server, set it within `config/production.json` as variable `mongodb` like:

    "mongodb": "mongodb://alinex_mongoadmin:mi2zae8Cai@localhost:31544"

## Add Client Code

The server itself works, but you should put the correct client also onto it. If you installed through npm this is already done else you should build the client as followed on a build machine:

```bash
$ curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
$ sudo dpkg --add-architecture i386 && sudo apt-get update
$ sudo apt-get install -y nodejs make zip git g++ mongodb wine-development wine32-development
$ wget https://dl.winehq.org/wine/wine-gecko/2.44/wine_gecko-2.44-x86_64.msi
$ wine-development msiexec /i wine_gecko-2.44-x86.msi
```

Then you can download the client code and install it:

```bash
$ git clone https://gitlab.com/alinex/node-portal-client
$ cd node-portal-client
$ npm install
```

To build and install the client into the server use:

```bash
$ API=https://demo.alinex.de:64850/ npm run build
$ DEPLOY=alinex@peacock.uberspace.de:/home/alinex/github/node-portal npm run deploy
```

This command will build the website and electron builds and deploy them on the admin server.

If you have installed the admin server locally from source beside the client, it is already connected with symlinks.

## Run server

Start the server from it's installation directory using:

```bash
$ npm start
```

Or use the global command (from the `bin` directory):

```bash
$ portal
```

To run it as a daemon you may call it using `nohup` or better use `pm2`.

You may also the environment settings to configure:

```bash
$ export NODE_ENV=development # or production
$ export NODE_CONFIG='{"loglevel":"verbose"}'
$ portal
```

The most common NODE_CONFIG seetings are:

-   `host` - use domain name or ip (default: localhost)
-   `port` - server port used, lower range (<1024) are only possible if started as root (default: 3030)
-   `loglevel` - 'error', 'warn', 'info', 'verbose', 'debug' or 'silly' (default on production: error)
-   `mongodb` - database connection (default: "mongodb://localhost:27017/alinexDB")

### Logs

The server logs will be written under `logs` directory:

-   `combined-yyyy-mm-dd.log`
-   `error-yyyy-mm-dd.log`

Older files will be automatically packed and removed after 14 days.

## Install Client

The client installation depends on the device:

-   Browser: access through the server root
-   Desktop: download from server and install
-   Mobile app: download from server (maybe later from play store and apple store)

!!! attention

    Like often in nodejs `NODE_ENV=development` or `NODE_ENV=production` will give you specific presets.
    Best way is to set this in `~/.bashrc` or `/etc/profile`.

Before installing the client you have to build it with the correct server URL:

```bash tab="development"
cd ../node-portal-client
NODE_ENV=development  # necessary to build on production
npm install
npm run build
API=https://demo.alinex.de:64850 npm run build
```

```bash tab="production"
cd ../node-portal-client
NODE_ENV=development  # necessary to build on production
npm install
API=https://demo.alinex.de:64850 npm run build
```

This will take some time.

To install the client on a server or within the admin server you can use the deploy script:

```bash
cd node-portal-client
NODE_ENV=development  # necessary to build on production
npm install
DEPLOY=alinex@peacock.uberspace.de:/home/alinex/code/node-portal npm run deploy
```

This will copy the client into the sub folder `public/web` and the downloads to `public/desktop` but feel free to move this static files anywhere they belong to.

## Security

Security is a critical point to use such system and as long as it accessible you should protect the system and it's data from unauthorized access internally and externally.

### Secure Protocol

The first point is to use the HTTPS protocol so that the data and URLs can be transferred without anyone watching it. This can be done by enabling HTTPS in the server directly or mostly better by using an offloading proxy like Nginx or Apache before which may also give you the ability of scaling using load balance technology.

### Access Rights

The next part for security is to setup the roles correctly and allow only what is really necessary to somebody. Also don't forget to disable accounts which are no longer working on that system or for your organization.

### Log Access

And at last consider logging each access and let your users know that you will log each access to data for security reasons.
