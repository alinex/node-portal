# Check Service

The check service is used to test that everything works fine and the server can be used.

The server has a list of checks defined which will run with timing as it is called and it's results be presented back to be easily analyzed for manual check or monitoring tools like Nagios.

The check service is accessible from local IP without authentication.

Run a complete check:

```bash
$ curl -sX GET http://localhost:3030/check | jq
```

```json
[
    {
        "check": "base",
        "status": true,
        "message": "Base check that server is running",
        "time": 0
    },
    {
        "check": "baseAsync",
        "status": true,
        "message": "Base check that server is running",
        "time": 0
    },
    ...
]
```

Or you may only run a single check:

```bash
$ curl -sX GET http://localhost:3030/check/base | jq
```

```json
{
    "check": "base",
    "status": true,
    "message": "Base check that server is running",
    "time": 0
}
```

The values are:

-   **check** - the shortname of the check which may be used to run only this
-   **status** - boolean `1` if OK else `0`
-   **message** - short text explaining the current status
-   **time** - time used to run check in milliseconds

To use it in Nagios you may check that there is no `status: 0` within the text. Or call a single check and use its `time` as performance data.

The following checks are defined:

| Check          | Description                         |
| -------------- | ----------------------------------- |
| base           | Check for sync service calls        |
| baseAsync      | Check for async service calls       |
| load           | Check current server load and lag   |
| mongo          | Check connection to mongo DB        |
| mongoUser      | Check at least one mongo user entry |
| mongoGuestRole | Check that guest role is defined    |
