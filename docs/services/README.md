# Available Services

The following services are available. This is a detailed documentation with described examples but you may also use the integrated Swagger interface for a short overview of the methods and to test them out.

-   [check](check.md)
-   [info](info.md)
-   [access](access.md)
-   [users](users.md)
-   [roles](roles.md)
-   [messages](messages.md)

A lot of services needs login before they can be used. This is done with the authentication service and to use it in shell we use the following method to login:

    $ export JWT=$(curl -sX \
      POST http://localhost:3030/authentication \
      -H 'Content-Type: application/json' \
      --data-binary '{ "strategy": "local", "email": "demo@alinex.de", "password": "demo123" }' \
      | jq -r .accessToken )

Further on the examples will use the `$JWT` variable to access restricted methods.
