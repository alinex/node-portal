# Roles Service

The role service will be used internally by the authorization service to get the users's abilities.
Also it is used within the access rights management through the client application.

Only for authenticated users.

## Model

The roles have the following fields:

-   `_id` - object id
-   `__v` - integer // version id
-   `name` - string
-   `description`? - string
-   `disabled`? - boolean // default is `false`
-   `abilities` - list
    -   `_id` - object id
    -   `actions` - 'read', 'update', 'create', 'remove'
    -   `subject` - name
    -   `conditions`? - { field: check, ... }
    -   `fields`? - name, ...
    -   `inverted`? - flag // default is `false`
    -   `reason`? - string // mainly to specify why user can't do something. See forbidden reasons for details
-   `createdAt` - date/time account created at
-   `updatedAt` - date/time the record was last changed

## find - list roles

    $ curl -H "Authorization: Bearer $JWT" -sX \
      GET http://localhost:3030/roles | jq

## get - role record

Get a list of users.

    $ curl -H "Authorization: Bearer $JWT" -sX \
      GET http://localhost:3030/users | jq

    {
        "total": 1,
        "limit": 10,
        "skip": 0,
        "data": [
            ...
        ]
    }

Get data of specific user:

    $ curl -H "Authorization: Bearer $JWT" -sX \
      GET http://localhost:3030/users/429ee71276122f55a3a94796 | jq

    {
        "_id": "429ee71276122f55a3a94796",
        "email": "demo@alinex.de",
        "nickname": "demo",
        "disabled": false,
        "createdAt": "Sat Mar 09 2019 22:41:28 GMT+0100 (CET)",
        "updatedAt": "Sat Mar 09 2019 22:41:28 GMT+0100 (CET)",
        "__v": 0,
        "name": "Demo User",
        "position": "Test",
        "avatar": "https://www.gravatar.com/avatar/dc23461c186acb6dacd8e6137543fdec?s=60&d=mm",
        "roles_id": [
            {
                "_id": "5b2025aedcb10b38733b9825",
                "abilities": [
                    {
                        "actions": [
                            "read",
                            "update",
                            "create",
                            "remove"
                        ],
                        "subject": [
                            "users",
                            "roles",
                            "info"
                        ],
                        "fields": [],
                        "_id": "5b2025aedcb10b38733b9826"
                    }
                ],
                "name": "Admin",
                "description": "Super Administrator with maximum rights",
                "createdAt": "2018-06-12T19:57:34.027Z",
                "updatedAt": "2018-06-12T19:57:34.027Z",
                "__v": 0
            }
        ]
    }

## create - new role

Create new role.

    $ curl -H "Authorization: Bearer $JWT" -sX \
      POST http://localhost:3030/roles \
      -H 'Content-Type: application/json' \
      --data-binary '{ "name" : "account-manager", "abilities": [{ "actions": ["read", "update"],  "subject": "users", "conditions": { "_id": "ME" } }] }' | jq

    {
        "_id": "5c896b04670dc32b160d4fca",
        "name": "account-manager",
        "abilities": [
            {
                "actions": [
                    "read",
                    "update"
                ],
                "subject": [
                    "users"
                ],
                "fields": [],
                "_id": "5c896b04670dc32b160d4fcb",
                "conditions": {
                    "_id": "ME"
                }
            }
        ],
        "createdAt": "2019-03-13T20:41:40.223Z",
        "updatedAt": "2019-03-13T20:41:40.223Z",
        "__v": 0
    }

## update - replace record

## patch - change some values

## remove - role
