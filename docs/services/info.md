# Info Service

This service is publicly available for debugging. It displays server information mainly used for debugging and analyzation.

## Model

The service will deliver record lists with:

-   **group** - string name like 'host', 'node', ...
-   **name** - string short name for the value
-   **value** - concrete value, mostly also string type

## find - information list

You can get a lot of server information using:

    $ curl -sX GET http://localhost:3030/info/ | jq .[0:2]
    [
        {
            "group": "host",
            "name": "architecture",
            "value": "x64"
        },
        {
            "group": "host",
            "name": "cpu type",
            "value": "Intel(R) Core(TM) i7-6500U CPU @ 2.50GHz"
        }
    ]

All in all this is a very large list but you may reduce it by selecting a specific
group of information:

    $ curl -sX GET http://localhost:3030/info/?group=node | jq
    [
        {
            "group": "node",
            "name": "process id",
            "value": 11030
        },
        {
            "group": "node",
            "name": "parent process id",
            "value": 11029
        },
        {
            "group": "node",
            "name": "process name",
            "value": "node"
        },
        {
            "group": "node",
            "name": "node version",
            "value": "11.11.0"
        },
        {
            "group": "node",
            "name": "v8 version",
            "value": "7.0.276.38-node.18"
        },
        {
            "group": "node",
            "name": "working directory",
            "value": "/home/alex/code/node-portal"
        },
        {
            "group": "node",
            "name": "process uptime (seconds)",
            "value": 959.706717939
        },
        {
            "group": "node",
            "name": "cpu usage (percent)",
            "value": 0.3
        },
        {
            "group": "node",
            "name": "memory rss (byte)",
            "value": 70033408
        },
        {
            "group": "node",
            "name": "memory virt (byte)",
            "value": 972591104
        },
        {
            "group": "node",
            "name": "event loop lag",
            "value": "0s 26ms"
        }
    ]

Using this method you can get a lot of information from the server. The Admin Application also has a info module which will show all of this in a pretty table.
