title: About

# Alinex Portal

The administration portal is an easy and fast way to manage IT systems for technical and non technical staff.

It is a universal control interface which can become real powerful by adding modules for special tasks. This modules will interact with the real systems in a proper and safe way. The user has no direct control but the necessary functionality as defined by the module.

![Architecture](architecture.svg)

The two main parts of the administration portal are the **client** which is loaded on the user's device and the **REST** server which works as the gateway to the resources like data, servuces abd other server machines.

## Features

**Client**

-   universal: web application, desktop and mobile app
-   intuitive and simple interface
-   fast and easy to use
-   modular structure
-   realtime communication
-   authorized parts only

**Server**

-   stateless using simple Java Web Token for authentication
-   websocket (Realtime API) or HTTP REST
-   service oriented architecture
-   authorization
-   server validation
-   detailed request, error and module logging
-   multiple database support

**Modules**

-   user management
-   role management (access rights)
-   chat application

## Showcase

Some example screenshots from the server:

![Server Start](assets/demo/server.start.png)
![API Docs](assets/demo/swagger.png)
![Logfiles](assets/demo/logtail.png)

### Web Client

Startup:

![Client Start](assets/demo/client.start.png)
![Client Login](assets/demo/client.login.png)

Administration:

![Edit Users](usage/users.png)
![Edit Roles](usage/users.png)

## Demo

Check out the demo on a small shared host: [demo.alinex.de](https://demo.alinex.de).
(Use the displayed placeholder text to login.)

## Development

As a developer you may find the additional information arround this project useful. So habe a look of the [overall documentation](https://alinex.gitlab.io/) which not only describes all used technologies but also the environment and used modules and ...
