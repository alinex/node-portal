# Roadmap

**What's coming next?**

That's a big question on a freelancer project. The roadmap may change any time or progress may be stuck. But hopefully I will come on in any way.

## Version 0.3.0

-   better documentation
-   updated server modules
-   enhanced logging
-   code optimization
-   JWT containing abilities

## Version 0.4.0

-   modernizing the client
-   using newer quasar framework

## Version 1.0.0

-   production ready and in use
