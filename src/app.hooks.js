// const logger = require('./hooks/logger')
const logger = require('./hooks/logger')
const { when } = require('feathers-hooks-common')
const authorize = require('./hooks/authorize')
const authenticate = require('./hooks/authenticate')

// Application hooks that run for every service
module.exports = {
  before: {
    all: [
      // first authenticate
      when(
        hook => hook.params.provider && `/${hook.path}` !== hook.app.get('authentication').path,
        authenticate
      ),
      logger,
      authorize()
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [logger],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [logger],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
}
