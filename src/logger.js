const winston = require('winston')
require('winston-daily-rotate-file')
const { createLogger, format, transports, addColors } = winston
const { combine, timestamp, printf } = format
const path = require('path')

const util = require('util')

function init() {
  return function() {
    let app = this

    const condensed = printf(info => {
      // eslint-disable-next-line no-control-regex
      const pad = ' '.repeat(7 - info.level.replace(/\u001b\[.*?m/g, '').length)
      return `${info.timestamp} [${info.level}${pad}] ${info.message}`
    })
    addColors({
      error: 'red',
      warn: 'yellow',
      info: 'green',
      verbose: 'blue',
      debug: 'cyan',
      silly: 'grey'
    })
    const logger = createLogger({
      level: app.get('loglevel'),
      format: combine(
        // format.colorize(),
        format.colorize({ all: true }),
        timestamp({ format: 'YYYY-MM-DD HH:mm:ss.SSS' }),
        condensed
      ),
      transports:
        app.get('logging') === 'file'
          ? [
            new transports.DailyRotateFile({
              dirname: 'logs',
              filename: 'combined-%DATE%.log',
              datePattern: 'YYYY-MM-DD',
              zippedArchive: true,
              maxFiles: '14d'
            }),
            new transports.DailyRotateFile({
              level: 'error',
              dirname: 'logs',
              filename: 'error-%DATE%.log',
              datePattern: 'YYYY-MM-DD',
              zippedArchive: true,
              maxFiles: '14d'
            })
          ]
          : [new transports.Console()]
    })

    app.set('logger', logger)
  }
}

function http(app, req, res, next) {
  const logger = app.get('logger')
  const ip = req.feathers.clientIP
  const start = new Date()
  logger.info(
    `${ip} ${req.protocol.toUpperCase()} before ${req.method} ${req.url} ${req.get('User-Agent')}`
  )
  logger.debug(
    `${ip} ${req.protocol.toUpperCase()} Header: ${util.inspect(req.headers, {
      breakLength: Infinity
    })}`
  )
  res.on('finish', function() {
    const end = new Date()
    logger.info(
      `${ip} ${req.protocol.toUpperCase()} after  ${req.method} ${req.url} ${res.statusCode} ${
        res.statusMessage
      } ${end - start} ms`
    )
  })
  next()
}

function body(app, req, res, next) {
  const logger = app.get('logger')
  const ip = req.feathers.clientIP
  // store original methods
  const oldWrite = res.write
  const oldEnd = res.end
  // collect data
  const chunks = []
  // intercept writing by also writing chunks in cache
  res.write = (...restArgs) => {
    chunks.push(Buffer.from(restArgs[0]))
    oldWrite.apply(res, restArgs)
  }
  // logging if response is complete
  res.end = (...restArgs) => {
    if (restArgs[0]) {
      chunks.push(Buffer.from(restArgs[0]))
    }
    let body = Buffer.concat(chunks)
      .toString('utf8')
      .replace(/\s+/g, ' ')
      // eslint-disable-next-line no-control-regex
      .replace(/[^\x00-\x7F]/g, '')
    const len = Buffer.concat(chunks).length
    if (body.length > 200) {
      body = body.substr(0, 200) + '...'
    }
    logger.debug(`${ip} ${req.protocol.toUpperCase()} Result: (${len} bytes) ${body}`)
    oldEnd.apply(res, restArgs)
  }
  next()
}

function notFound(app, req, res) {
  const logger = app.get('logger')
  const ip = req.feathers.clientIP

  logger.warn(`${ip} ${req.protocol.toUpperCase()} error  Page not found ${req.url}`)

  res.status(404)
  const type = req.headers.accept
  if (type && type == 'application/json') {
    res.json({
      name: 'NotFound',
      message: 'Page not found',
      code: 404,
      errors: {}
    })
  } else {
    res.sendFile(`${path.dirname(__dirname)}/public/error/404.html`)
  }
}

function error(app, err, req, res) {
  const logger = app.get('logger')
  const ip = req.feathers.clientIP

  const stck = String(err.stack || err)
    .split('\n')
    .filter(function(s) {
      return !String(s).match(/\/node_modules\// && String(s).match(/\//))
    })
  const isProd = process.env.NODE_ENV === 'production'
  const response = err || {
    name: 'InternalServerError',
    message: 'Internal server error',
    code: 500,
    errors: {},
    stack: isProd ? [] : stck
  }

  logger.error(`${ip} ${req.protocol.toUpperCase()} error  ${err}`)
  stck.forEach(e => logger.debug(e))

  res.status(response.code)
  const type = req.headers.accept
  if (type && type == 'application/json') {
    res.json(response)
  } else {
    res.sendFile(`${path.dirname(__dirname)}/public/error/500.html`)
  }
}

function hook(context, name) {
  const { method, path, id } = context
  const logger = context.app.get('logger')
  const ip = context.params.clientIP || '-'
  const type = context.type == 'before' ? 'before' : 'after '
  logger.debug(`${ip} HOOK ${type} ${method.toUpperCase()} /${path}${id ? '/' + id : ''}: ${name}`)
}

module.exports = { init, http, body, notFound, error, hook }
module.exports.default = { init, http, body, notFound, error, hook }
