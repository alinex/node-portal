const express = require('@feathersjs/express')
const bodyParser = require('body-parser')
const helmet = require('helmet')
const favicon = require('serve-favicon')
const compress = require('compression')
const cors = require('cors')

const feathers = require('@feathersjs/feathers')
const configuration = require('@feathersjs/configuration')
const socketio = require('@feathersjs/socketio')

const middleware = require('./middleware')
const services = require('./services')
const appHooks = require('./app.hooks')
const channels = require('./channels')
const logger = require('./logger')

const logo = require('./logo')
const path = require('path')

const app = express(feathers())

// Load app configuration
app.configure(configuration())
app.configure(logger.init())

// eslint-disable-next-line no-console
console.log(logo('Portal Application'))

// use first IP instead of localhost on development
if (process.env.NODE_ENV != 'production' && app.get('host') == 'localhost') {
  const os = require('os')
  const ifaces = os.networkInterfaces()
  for (var dev in ifaces) {
    // ... and find the one that matches the criteria
    var iface = ifaces[dev].filter(function(details) {
      return details.family === 'IPv4' && details.internal === false
    })
    if (iface.length > 0) {
      app.set('host', iface[0].address)
      break
    }
  }
}
app.set('trust proxy', 'loopback')

// Set up Plugins and providers
app.configure(express.rest())
app.configure(
  socketio(io => {
    io.use((socket, next) => {
      socket.feathers.clientIP = socket.handshake.address
      next()
    })
  })
)

// Enable CORS, security, compression, favicon and body parsing
app.use(cors())
app.use(helmet())
app.use(compress())
app.use(bodyParser.json({ limit: '50mb', type: 'application/json' }))
app.use(
  bodyParser.urlencoded({
    limit: '50mb',
    extended: true,
    parameterLimit: 50000,
    type: 'application/x-www-form-urlencoding'
  })
)

// Bridge request to feathers
app.use(function(req, res, next) {
  req.feathers.clientIP =
    req.headers['x-forwarded-for'] ||
    req.connection.remoteAddress ||
    req.socket.remoteAddress ||
    req.connection.socket.remoteAddress
  next()
})

// basic http logging
app.use((req, res, next) => logger.http(app, req, res, next))
app.use((req, res, next) => logger.body(app, req, res, next))

// Host the public folder
app.use(favicon(path.join(app.get('public'), 'favicon.ico')))
app.use('/', express.static(app.get('public')))
// Configure other middleware (see `middleware/index.js`)
app.configure(middleware)
// Set up our services (see `services/index.js`)
app.configure(services)
// Set up event channels (see channels.js)
app.configure(channels)

app.hooks(appHooks)

// log returning error response as JSON
app.use((req, res, next) => logger.notFound(app, req, res, next))
app.use((err, req, res, next) => logger.error(app, err, req, res, next))

module.exports = app
