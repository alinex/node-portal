const { hashPassword, protect } = require('@feathersjs/authentication-local').hooks
const { discard } = require('feathers-hooks-common')
const populate = require('../../hooks/populate')
const depopulate = require('../../hooks/depopulate')
const gravatar = require('../../hooks/gravatar')

module.exports = {
  before: {
    all: [depopulate()],
    find: [],
    get: [],
    create: [hashPassword(), gravatar()],
    update: [hashPassword(), gravatar()],
    patch: [hashPassword(), gravatar()],
    remove: []
  },

  after: {
    all: [protect('password')],
    find: [populate(), discard('_include')],
    get: [populate(), discard('_include')],
    create: [],
    update: [],
    patch: [populate(), discard('_include')],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
}
