const swagger = require('feathers-swagger')

const pjson = require('../../package.json')

module.exports = function(app) {
  app.configure(
    swagger({
      info: {
        title: 'Admin Portal REST API',
        description:
          'This is the API documentation and can also be used to test the REST server by calling single services.',
        version: pjson.version // get version from package.json
      },
      host: `${app.get('host')}:${app.get('port')}`,
      docsPath: '/swagger',
      uiIndex: true,
      // authentication
      //      tags: [
      //        {
      //          name: "authorization",
      //          description: "Check that everything works fine on the server.",
      //          externalDocs: {}
      //        }
      //      ],
      //      paths: [
      //        {
      //          "/authorization": {
      //          }
      //        }
      //      ],
      security: [
        {
          bearer: []
        }
      ],
      securityDefinitions: {
        bearer: {
          type: 'apiKey',
          name: 'Authorization',
          description: 'Get an API key by authorization, which is used for further calls.',
          in: 'header'
        }
      }
    })
  )
}
