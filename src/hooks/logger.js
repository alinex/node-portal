// A hook that logs service method before, after and error
const util = require('util')

module.exports = async function authenticate(context) {
  const { app, type, params, path, method, id, data, result, error } = context
  const logger = app.get('logger')

  const level = params.provider ? 'info' : 'verbose'
  const user = params.payload ? params.payload.userId : ''
  const ip = params.clientIP || '-'
  const provider = params.provider ? params.provider.toUpperCase() : 'INTERNAL'

  switch (type) {
  case 'before':
    logger[level](
      `${ip} ${provider} before ${method.toUpperCase()} /${path}${id ? '/' + id : ''}`
    )
    user && logger.debug(`${ip} User: ${user}`)
    // already logged in HTTP
    //    if (params.headers && Object.keys(params.headers).length) {
    //      logger.debug(
    //        `${ip} ${provider} Header: ${util.inspect(params.headers, { breakLength: Infinity })}`
    //      )
    //    }
    if (params.query && Object.keys(params.query).length) {
      logger.debug(
        `${ip} ${provider} Parameter: ${util.inspect(params.query, { breakLength: Infinity })}`
      )
    }
    if (data && Object.keys(data).length) {
      logger.debug(`${ip} ${provider} Data: ${util.inspect(data, { breakLength: Infinity })}`)
    }
    break
  case 'after':
    logger[level](
      `${ip} ${provider} after  ${method.toUpperCase()} /${path}${id ? '/' + id : ''}`
    )
    user && logger.debug(`${ip} User: ${user}`)
    if (result && Object.keys(result).length) {
      logger.debug(`${ip} ${provider} Result: ${util.inspect(result, { breakLength: Infinity })}`)
    }
    break
  case 'error':
    logger.error(
      `${user} ${provider} ${method.toUpperCase()} /${path}${id ? '/' + id : ''} ${error.stack}`
    )
    user && logger.debug(`${ip} User: ${user}`)
  }

  return context
}
