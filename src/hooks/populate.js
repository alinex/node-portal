const loggerHook = require('../logger').hook

const find = async (context, service, record) => {
  const clientIP = context.params.clientIP
  if (typeof record === 'string' || record._bsontype == 'ObjectID') {
    const res = await context.app.service(service).find({ query: { _id: record[0] }, clientIP })
    return res.data[0]
  } else if (Array.isArray(record)) {
    const list = record.filter(e => typeof e === 'string' || e._bsontype == 'ObjectID')
    if (list.length === 0) return record
    const res = await context.app.service(service).find({ query: { _id: { $in: list } }, clientIP })
    return res.data
  }
  return record // context.app.service(service).find()
}

module.exports = function(options = {}) {
  return async context => {
    loggerHook(context, 'populate')
    const { method, data, type } = context

    // get records
    let items = type === 'before' ? data : context.result
    items = items && method === 'find' ? items.data || items : items
    if (!items) return context // skip because no data

    // populate
    if (!Array.isArray(items)) items = [items]
    if (Object.keys(options).length === 0) {
      // automatic if no options
      for (const el of items) {
        for (const key of Object.keys(el)) {
          const m = key.match(/^(\w+)_id/)
          if (m) el[key] = await find(context, m[1], el[key])
        }
      }
      return context
    } else {
      // use given field
      throw new Error('populate with setup not implemented, yet.')
    }
  }
}
