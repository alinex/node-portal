// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
const loggerHook = require('../logger').hook

module.exports = function() {
  // eslint-disable-line no-unused-vars
  return async context => {
    loggerHook(context, 'process-message')
    const { params, data } = context

    // Throw an error if we didn't get a text
    if (!data.text) {
      throw new Error('A message must have a text')
    }

    // The authenticated user
    const user = params.user
    // The actual message text
    const text = data.text
      // Messages can't be longer than 400 characters
      .substring(0, 400)
      // Do some basic HTML escaping
      .replace(/&/g, '&amp;')
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;')
    // Override the original data (so that people can't submit additional stuff)
    context.data = {
      text,
      // Set the user id
      user_id: user._id
    }
    // Best practise, hooks should always return the context
    return context
  }
}
