const { Ability } = require('@casl/ability')
const { toMongoQuery } = require('@casl/mongoose')
const { Forbidden } = require('@feathersjs/errors')
const loggerHook = require('../logger').hook
const util = require('util')

const TYPE_KEY = Symbol.for('type')

// aliases for feathers service methods
Ability.addAlias('update', 'patch')
Ability.addAlias('read', ['get', 'find'])
Ability.addAlias('remove', 'delete')

// detect service name from object
function subjectName(subject) {
  if (!subject || typeof subject === 'string') {
    return subject
  }
  return subject[TYPE_KEY]
}

function canReadQuery(query) {
  return query !== null
}

function replaceConditionVariables(condition, context) {
  if (!condition) return null
  Object.keys(condition).forEach(key => {
    if (typeof condition[key] === 'object') {
      replaceConditionVariables(condition[key], context)
    } else if (condition[key] === '$MYSELF') {
      condition[key] = context.params.user ? context.params.user._id : '-'
    }
  })
}

// hook to check if user is allowd
module.exports = function authorize(name = null) {
  return async function(context) {
    // get meta data
    const action = context.method
    const service = name ? context.app.service(name) : context.service
    const serviceName = name || context.path
    const logger = context.app.get('logger')
    // no checking for internal calls or authentication
    if (!context.params.provider || context.path == 'authentication') {
      return context
    }

    loggerHook(context, 'authenticate')

    // check is allowed without rights for local ip
    if (
      ['check'].includes(context.path) &&
      ['127.0.0.1', '::ffff:127.0.0.1'].includes(context.params.clientIP)
    ) {
      return context
    }
    // get abilities
    let rules = []
    if (context.params.user) {
      const roles = context.params.user.roles_id
      rules = [].concat.apply([], roles.filter(role => !role.disabled).map(role => role.abilities))
    } else {
      // load guest role
      const roleModel = require('../models/roles')(context.app)
      rules = (await roleModel.find({ name: 'Guest' }))[0].abilities
    }
    // interpret conditions
    rules.map(rule => replaceConditionVariables(rule.conditions, context))
    rules.forEach(e => logger.debug('Ability: ' + util.inspect(e, { breakLength: Infinity })))
    const ability = new Ability(rules, { subjectName })
    // helper to check rights
    const throwUnlessCan = (action, resource) => {
      if (ability.cannot(action, resource)) {
        throw new Forbidden(`You are not allowed to ${action} ${serviceName}`)
      }
    }
    // add abilities to params
    context.params.ability = ability
    // check for create and also authorization
    if (context.method === 'create') {
      context.data[TYPE_KEY] = serviceName
      throwUnlessCan('create', context.data)
      return context
    }
    // direct method call without element id
    if (!context.id) {
      // check abilities
      //throwUnlessCan(action);
      const query = toMongoQuery(ability, serviceName, action)
      if (canReadQuery(query)) {
        Object.assign(context.params.query, query)
      } else {
        // The only issue with this is that user will see total amount of records in db
        // for the resources which he shouldn't know.
        // Alternative solution is to assign `__nonExistingField` property to query
        // but then feathers-mongoose will send a query to MongoDB which for sure will return empty result
        // and may be quite slow for big datasets
        context.params.query.$limit = 0
      }
      return context
    }

    // get object to check it's data
    const params = Object.assign({}, context.params, { provider: null })
    const result = await service.get(context.id, params)
    // set service name to object
    result[TYPE_KEY] = serviceName
    // check abilities
    throwUnlessCan(action, result)
    // if it is a simple get call we can just use the already retrieved result
    if (action === 'get') {
      context.result = result
      logger.verbose('use already fetched result')
    }

    return context
  }
}
