const loggerHook = require('../logger').hook

module.exports = function() {
  return context => {
    loggerHook(context, 'user-abilities')

    context.data.abilities = []

    return context
  }
}
