const loggerHook = require('../logger').hook

const find = async (context, service, record) => {
  if (typeof record === 'string' || record._bsontype == 'ObjectID') {
    return
  } else if (Array.isArray(record)) {
    const list = record.filter(e => e._id)
    if (list.length === 0) return record
    return list.map(e => e._id)
  } else if (record._id) {
    return record._id
  }
  return record // context.app.service(service).find()
}

module.exports = function(options = {}) {
  return async context => {
    loggerHook(context, 'depopulate')
    const { method, data, type } = context
    const logger = context.app.get('logger')

    // get records
    let items = type === 'before' ? data : context.result
    items = items && method === 'find' ? items.data || items : items
    if (!items) return context // skip because no data

    // depopulate
    if (!Array.isArray(items)) items = [items]
    if (Object.keys(options).length === 0) {
      // automatic if no options
      for (const el of items) {
        for (const key of Object.keys(el)) {
          const m = key.match(/^(\w+)_id/)
          logger.debug(`populate ${key}`)
          if (m) el[key] = await find(context, m[1], el[key])
        }
      }
      return context
    } else {
      // use given field
      throw new Error('populate with setup not implemented, yet.')
    }
  }
}
