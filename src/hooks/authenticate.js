const { authenticate } = require('@feathersjs/authentication').hooks
const { NotAuthenticated } = require('@feathersjs/errors')
const loggerHook = require('../logger').hook

const verifyIdentity = authenticate('jwt')

function hasToken(hook) {
  if (hook.params.headers == undefined) return false
  if (hook.data.accessToken == undefined) return false
  return hook.params.headers.authorization || hook.data.accessToken
}

module.exports = async function authenticate(context) {
  loggerHook(context, 'authenticate')
  try {
    await verifyIdentity(context)
  } catch (error) {
    if (error instanceof NotAuthenticated && !hasToken(context)) {
      return context
    }
  }
  const user = context.params.user
  if (user && user.disabled) {
    throw new Error('This user has been disabled')
  }
  return context
}
